<?php
/**
 * @file
 * webstocks_lottery_feature.context.inc
 */

/**
 * Implementation of hook_context_default_contexts().
 */
function webstocks_lottery_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'Lottery';
  $context->description = '';
  $context->tag = 'type';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'lottery' => 'lottery',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'webstocks_lottery-total_prize' => array(
          'module' => 'webstocks_lottery',
          'delta' => 'total_prize',
          'region' => 'content_header',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('type');
  $export['Lottery'] = $context;

  return $export;
}
