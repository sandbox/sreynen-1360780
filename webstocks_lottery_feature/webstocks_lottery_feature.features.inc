<?php
/**
 * @file
 * webstocks_lottery_feature.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function webstocks_lottery_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_node_info().
 */
function webstocks_lottery_feature_node_info() {
  $items = array(
    'lottery' => array(
      'name' => t('Lottery'),
      'base' => 'node_content',
      'description' => t('A question, with multiple guess answers. Winners matching correct answer split bets.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
